file = open("table.html","a")
print("Pointer: ", file.tell())

n = int(input("Enter Number: "))
r=""
for i in range(1,11):
    r += "<h1 style='color:red;text-align:center;'>{} x {} = <span style='color:green'>{}</span></h1>".format(n,i,n*i)
r+="<hr/>"
file.write(r)
print("Pointer: ", file.tell())
print("File written successfully!")
file.close()

# print(file.write('h1')) ##We can apply any operation to closed file
