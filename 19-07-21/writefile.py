file = open("abcd.txt", "w")

print("Pointer: ", file.tell())
# file.write("Hello, How are you?")
n = int(input("Enter a number: "))
for i in range(1,11):
    file.write("{} x {} = {}\n".format(n,i,n*i))

print("Pointer: ", file.tell())

file.close()