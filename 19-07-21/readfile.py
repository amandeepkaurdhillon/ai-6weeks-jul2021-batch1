#open file 
# file = open("notes.txt","r")
file = open("notes.txt","rb")

#read content of a file 
# print(file.read())
# print(file.read(10))
# print(file.readline())
# print(file.readline())

# l = file.readlines()
# print(l[-1])

a = file.read()
print(type(a))
print(a)

# file.write('hello') ## It will return an error, because mode is 'read'

#close file 
file.close()
