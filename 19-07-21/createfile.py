import  os
import requests
try:
    city = input("Enter City Name: ")
    path = "cities/{}.html".format(city)
    file = open(path,"x")
    API_URL = 'https://api.openweathermap.org/data/2.5/forecast?q={}&appid=afab88ec0031e60546ddc02ab8577504&units=metric'.format(city)
    data = requests.get(API_URL).json()
    # print(data)
    result = ""
    for w in data["list"]:
        # if w['dt_txt'].split()[1]=="09:00:00":
        result+="<div style='text-align:center;box-shadow:0px 0px 20px black;width:20%;padding:1%;margin:10px;float:left;'>"
        result+="<img src='https://openweathermap.org/img/wn/{}@2x.png'>".format(w['weather'][0]['icon'])
        result+="<h1>{}</h1>".format(w['dt_txt'])
        result+="<p>Temp:{}°C</p>".format(w['main']['temp'])
        result+="<p><em>{} ({})</em></p>".format(w['weather'][0]['main'],
        w['weather'][0]['description'])
        result+="</div>"
        
    file.write(result)
    # f.write("TEST DATA")
    # f.close()
except FileExistsError:
    # os.remove('demo.txt')  #to delete a file
    print("A file with this name already exists, ")