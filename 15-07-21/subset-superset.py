a = {10,30,45,20}
b = {10,20}

if b.issubset(a):
    print("b is subset of a")
else:
    print("b is not subset of a")

c = a.issuperset(b)
print(c)