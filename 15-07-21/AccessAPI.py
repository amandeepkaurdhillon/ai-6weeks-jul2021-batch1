#Install requests 
# pip install requests
import requests

data = requests.get('https://restcountries.eu/rest/v2/all').json()
c = 1
for i in data:
    print('{}. {}'.format(c,i['name']))
    print('\t{}'.format(i['population']))
    print('\t{}'.format(i['capital']))
    print('\t{}'.format(i['borders']))
    c+=1
    print('-------------')