languages = ["HTML","Python","Java","Java Script",
"React","PHP"]
print(len(languages))

#Indexing
print(languages[1])
print(languages[-2 ])

#Slicing 
print(languages[1:])
print(languages[:2])
print(languages[:-2])
print(languages[-2:])

#Stepping
a = "0123456789"
print(languages[::1])
print(languages[::2])
print(languages[::-1])
print(a[::-1])