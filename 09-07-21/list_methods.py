a = ["red","green","blue"]
b = ["yellow","cyan","magenta"]

#Concatenation
print(a+b)
#Iteration
print(a*3)
#Membership
print("black" in a)
print("black" not in a)

print("Before: ",a)
#UPDATE
# a[1] = "white"

#Insert
# a.append("pink")
# a.insert(2,"maroon")

#delete
# a.pop()
a.remove("green")

print("After: ",a)

# print(dir(a))


n = [13,5,100,200,4,123]
ab = [20,23]

# n.sort()
# n.reverse()
# print(n)
# n.append(ab)
n.extend(ab)
print(n)
# print(n[6][1])