a = "Python was created by Guido van rossum"

print(a)
print(a.upper())
print(a.lower())
print(a.title())
print(a.capitalize())
print(a.swapcase())

b = "456788adf"
print(b.isupper())
print(b.islower())
print(b.isalpha())
print(b.isalnum())
print(b.isdigit())

# print(dir(b))

c = "Admin@1231@"
print(c.count("z"))
print(c.index("@",6))

d = "red green blue magenta orange"
print(d[0])
print(d.split()[0])
print(d.split("r"))