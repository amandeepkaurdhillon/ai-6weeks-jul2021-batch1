import tkinter as tk 

win = tk.Tk()
win.title("MY OPTIONS")
win.geometry("300x300")

menubar = tk.Menu(win)
def new_win():
    r = tk.Tk()
    r.title("Untitled")
    r.config(bg="yellow")
    r.geometry("500x500")
    tk.Label(r,text="WELCOME TO NEW WINDOW",fg="white",bg="green",font=("arial",20)).pack(fill="x")
    r.mainloop()

def abcd():
    lbl.pack()
    lbl.config(text="Welcome To This Window")

filem = tk.Menu(menubar,tearoff=0)
filem.add_command(label="New",command=new_win)
filem.add_command(label="Open...",command=abcd)
filem.add_command(label="Save")
filem.add_command(label="Save As")
filem.add_separator()
filem.add_command(label="Exit...",command=win.destroy)
# filem.add_command(label="Exit...",command=exit)

editm = tk.Menu(menubar,tearoff=0)
editm.add_command(label="Undo")
editm.add_separator()
editm.add_command(label="Cut")
editm.add_command(label="Copy")
editm.add_command(label="Paste")

menubar.add_cascade(label="File", menu=filem)
menubar.add_cascade(label="Edit", menu=editm)

lbl = tk.Label(win, bg="red",fg="white",font=("courier new",18))

win.config(menu=menubar)
win.mainloop()