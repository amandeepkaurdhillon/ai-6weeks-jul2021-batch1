import tkinter as tk 
from tkinter import messagebox
main = tk.Tk()
main.title("Tkinter Listbox")
main.minsize(300,300)
def add_item():
    item = en.get()
    index = ls.size()
    ls.insert(index,item)
def view_item():
    i = ls.curselection()
    item=ls.get(i)
    messagebox.showinfo("selected","Selected Item: {}".format(item))
def del_item():
    i = ls.curselection()
    name = ls.get(i)
    item=ls.delete(i)
    messagebox.showinfo("selected","{} deleted succesfully!".format(name))

label = tk.Label(main, text="Create Shopping List", bg="orangered",
fg="white", font=("cursive",20))
label.pack(padx=20, ipady=5)
en =  tk.Entry(main)
en.pack(pady=10,fill="x", padx=20,ipady=10) 
btn = tk.Button(main,text="Add Item", fg="white", bg="green", relief="flat",
command=add_item)
btn1 = tk.Button(main,text="View Item", fg="white", bg="orange", relief="flat",
command=view_item)
btn2 = tk.Button(main,text="Delete Item", fg="white", bg="red", relief="flat",
command=del_item)

btn.pack(fill="x",padx=20,pady=3)
btn1.pack(fill="x",padx=20,pady=3)
btn2.pack(fill="x",padx=20,pady=3)
ls = tk.Listbox(main)
ls.pack(fill="x", padx=20)
# ls.insert(1,"Red")
# ls.insert(2,"Green")
# ls.insert(3,"Blue")
# for i in range(1,100):
#     ls.insert(i,i)
# print("Total:", ls.size())

main.mainloop()