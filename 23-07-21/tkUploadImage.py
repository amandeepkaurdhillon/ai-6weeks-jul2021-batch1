import tkinter as tk 
from tkinter import filedialog 
from PIL import Image, ImageTk 
import shutil
class MyWindow:
    def __init__(self):
        self.win = tk.Tk()
        self.win.title("UPLOAD IMAGE IN TK")
        self.win.geometry("500x500")

        self.lf = tk.LabelFrame(self.win, text="Open a file")
        self.lf.grid(row=0,column=0,padx=20, pady=30, ipadx=20, ipady=20)

        self.btn = tk.Button(self.lf, text="Browse a file",command=self.upload_image,bg="green",fg="white")
        self.btn.grid(row=0, column=1)
        self.lbl = tk.Label(self.lf) 
        self.lbl.grid(row=2,column=0,columnspan=2)

    def upload_image(self):
        # path = "C:/Users/SachTech/Pictures"
        path="/"
        self.file_name = filedialog.askopenfilename(initialdir=path,title="Select a file",
        filetype=(("jpg","*.jpg"),("png","*.png"),))
        self.label = tk.Label(self.lf,text=self.file_name).grid(row=1,column=0)
        self.display_image()

    def display_image(self):
        img = Image.open(self.file_name)
        image = ImageTk.PhotoImage(img)
        self.lbl.config(image=image)
        self.lbl.image = image
        shutil.copy(self.file_name,"uploads")
        # shutil.move(self.file_name,"uploads")

if __name__=="__main__":
    obj = MyWindow()
    obj.win.mainloop()