#Run following command: pip install pillow
import tkinter as tk 
from PIL import Image, ImageTk

root = tk.Tk()
root.title("Display Image in Tk")
# path="C:/Users/SachTech/Documents/PY-SixWeeks-July2021/images/download.jpg"
path="laptop.jpg"
img = Image.open(path)
image = ImageTk.PhotoImage(img)

lbl = tk.Label(root,image=image)
lbl.image = image 
lbl.pack()

root.mainloop()