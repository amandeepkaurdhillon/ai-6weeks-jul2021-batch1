import tkinter as tk  

#creating a tkinter window
win = tk.Tk()

#Configure tk window
win.title("My Window")
win.geometry("500x500")

#restrict resize
win.resizable(0,True)
#display tkinter window
win.mainloop()

