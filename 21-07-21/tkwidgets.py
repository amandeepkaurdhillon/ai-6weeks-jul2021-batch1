import tkinter as tk  

#creating a tkinter window
win = tk.Tk()

#Configure tk window
win.title("My Window")
win.geometry("500x500")
win.config(bg="#610dfc")
## Adding widgets

lb = tk.Label(win, text="This is my first Tkinter Window",
font=('Arial',24), fg="red", bg="yellow")
lb.pack(fill="x", pady=20)

lb = tk.Label(win, text="WELCOME EVERYONE!",
font=('Courier New',18), fg="#610dfc", bg="#ffffff")
lb.pack(fill="x", padx=100,ipadx=100, ipady=100)

#restrict resize
# win.resizable(0,True)
#display tkinter window
win.mainloop()

