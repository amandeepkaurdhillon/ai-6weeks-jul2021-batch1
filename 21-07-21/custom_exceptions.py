class MyError(Exception):
    def __init__(self, d='First variable must be greater'):
        self.d = d 
    def __str__(self):
        return self.d
try:   
    a = int(input("Enter First Number: "))
    b = int(input("Enter Second Number: "))
    if a < b:
        raise MyError
    else:
        print("Subtraction: ", a-b)
except MyError as err:
    print("Error occurrded!")
    print(err)
