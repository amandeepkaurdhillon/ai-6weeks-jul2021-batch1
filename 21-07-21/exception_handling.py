try:
    name = "python"
    print(name)
    n = {'name':'peter','age':20}
    print(n)
    import abcd
    # print(n['r_no'])
    # print(name[10])
    # print(age)
    print('hello there')
except NameError:
    print("Please check variable names carefully!")
except IndexError:
    print("Please check Indexes!")
except:
    print("OOPs! Something went wrong!")