import random
options = '''
    Press 1: To create account
    Press 2: To check  balance
    Press 3: To deposit
    Press 4: To withdraw
    Press 0: To exit
'''
print(options)
all_users = []
def create_account():
    name = input("Enter Your Name: ")
    amt = float(input("Enter Initial Amount: "))
    pin = random.randint(1000,9999)
    user = {'name':name,'balance':amt,'pin':pin}
    all_users.append(user)
    print("Dear {} Your account created successfully! Pin:{}\n".
    format(name,pin))
    
def check_balance():
    pin_to_match = int(input("Enter Your Pin: "))
    match = False
    for user in all_users:
        if user['pin'] == pin_to_match:
            print("Welcome {}!".format(user['name']))
            print('Here is your current balance Rs.{}/-\n'.
            format(user['balance']))
            match = True
    if match==False:
        print("Sorry You are not registered with us!\n")

def deposit():
    pin_to_match = int(input("Enter Your Pin: "))
    match = False
    for user in all_users:
        if user['pin'] == pin_to_match:
            print("Welcome {}!".format(user['name']))
            amount = float(input("Enter amount to deposit: "))
            user['balance'] += amount
            print("Your account credited with Rs.{}/-\n".
            format(amount))
            match = True
    if match==False:
        print("Sorry You are not registered with us!\n")
def withdraw():
    pin_to_match = int(input("Enter Your Pin: "))
    match = False
    for user in all_users:
        if user['pin'] == pin_to_match:
            print("Welcome {}!".format(user['name']))
            amount = float(input("Enter amount to deposit: "))
            if amount <= user['balance']:
                user['balance'] -= amount
                print("Your account debited with Rs.{}/-\n".
                format(amount))
            else:
                print("You don't have sufficient balance to make this transaction!\n")
            match = True
    if match==False:
        print("Sorry You are not registered with us!\n")

while True:
    c = input("Enter Your Choice: ")
    if c=="1":
        create_account() 
    elif c=="2":
        check_balance()
    elif c=="3":
        deposit() 
    elif c=="4":
        withdraw()
    elif c=="0":
        break
    else:
        print("Invalid Choice!")