def multiply(*args):
    m = 1
    s =''
    for i in args:
        m*=i
        s+='{} x '.format(i) 
    print('{} = {}'.format(s[:-2],m))

multiply(1,1,1)
multiply(10,20)
multiply(123,45,56,67,7,8,)
multiply(2,34)

# 1*1*1 = 1
# 1*2*3 = 6