def Area(r=1, pi=3.14):
    print('pi= {} r={} area={}'.format(
        pi, r, pi*r**2
    ))

Area(1,3.14)
Area(100)
Area()