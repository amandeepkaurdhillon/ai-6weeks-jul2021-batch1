def employee(name, age, salary):
    print('Name: ',name)
    print('Age: ',age)
    print('Salary: ',salary,'\n')

#positional arguments
employee('James',20,10000)
employee(20,'peter',30000)

#keyword arguments
employee(age=20,name='peter',salary=30000)
employee('peter', salary=50000,age=22)