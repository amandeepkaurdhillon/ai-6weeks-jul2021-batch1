import tkinter as tk
from tkinter import *
from tkinter import messagebox
class MyWindow:
    def __init__(self):
        self.main = tk.Tk()
        self.main.title("Tkinter Text Widget")
        self.main.geometry("500x800")
        self.main.config(bg="#5c1033")

        self.lb_email = tk.Label(self.main,text="Enter Email Address",bg="#5c1033",fg="#ffffff") 
        self.email = tk.Entry(self.main) 
        self.lb_msz = tk.Label(self.main,text="Enter Message Here",bg="#5c1033",fg="#ffffff") 
        self.msz = tk.Text(self.main) 
        self.submit = tk.Button(self.main, bg="green",fg="white",
        text="SEND MESSAGE",width=50,command=self.get_value)

        self.lb_email.place(x=50,y=50)
        self.email.place(x=170,y=50)
        self.lb_msz.place(x=50,y=80)
        self.msz.place(x=170,y=80)
        self.submit.place(x=170,y=500)

    def get_value(self):
        email = self.email.get()
        val = self.msz.get("1.0",END)
        self.msz.insert(INSERT,"")
        print("data=",val)
        #Export data to file
        f = open("messages.csv","a")
        f.write("{},{}".format(email,val))
        f.close()
        messagebox.showinfo("success","File saved successfully!")
if __name__=="__main__":
    obj = MyWindow()
    obj.main.mainloop()


