import tkinter as tk
from tkinter import font

win = tk.Tk()
win.title("Tkinter Radio Button")
win.geometry("300x500")
# gender = tk.IntVar()
gender = tk.StringVar()

def abcd(): #radio
    val = gender.get()
    r = "Selected gender: {}".format(val)
    result.config(text=r)
def getValue(): #check
    # c1.select()
    # c2.deselect()
    r = hob.get()
    t = tr.get()
    print(r)
    print(t)
    

lbl = tk.Label(win, text="Enter Contact Number: ")
en = tk.Entry(win)
lbl1 = tk.Label(win, text="Enter Password: ")
en1 = tk.Entry(win,show="*")

lbl2 = tk.LabelFrame(win, text="Select Gender: ")
r1 = tk.Radiobutton(lbl2,value="male",variable=gender,text="Male",
command=abcd )
r2 = tk.Radiobutton(lbl2,value="female",variable=gender ,text="Female",
command=abcd)

hobbies =tk.LabelFrame(win, text="Select Hobbies: ")
hob = tk.IntVar()
tr = tk.StringVar()

c1 = tk.Checkbutton(hobbies,text="Reading Books",offvalue=0,onvalue=1,variable=hob)
c2 = tk.Checkbutton(hobbies,text="Travelling",offvalue="0",onvalue="travelling",variable=tr)
c3 = tk.Checkbutton(hobbies,text="Music")

btn = tk.Button(win,text="Click to Continue",bg="green",fg="white"
,relief="flat",width=35,command=getValue)

lbl.grid(row=0, column=0, pady=10,padx=10)
en.grid(row=0, column=1, pady=10,padx=10)
lbl1.grid(row=1, column=0, pady=10,padx=10)
en1.grid(row=1, column=1, pady=10,padx=10)
lbl2.grid(row=2, columnspan=2,ipadx=80, pady=10,padx=10)
r1.pack()
r2.pack()
result = tk.Label(lbl2,fg="green",font=('Arial',10))
result.pack()

hobbies.grid(row=3,column=0,columnspan=2,ipadx=80, pady=10,padx=10)
c1.pack()
c2.pack()
c3.pack()

btn.grid(row=4,columnspan=2)

## Add geometry manager
win.mainloop()