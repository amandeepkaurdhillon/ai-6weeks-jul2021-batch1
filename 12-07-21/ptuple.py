a = 10,20,30

print(type(a))
print(a)
print(len(a))

b = ("Monday","Tuesday","Wednesday", "Thursday",
"Friday")
#Indexing]
print(b[1])
print(b[-1])


#Slicing
print(b[2:])
print(b[-2:])
print(b[:-2])

#Stepping
print(b[::1])
print(b[::2])
print(b[::-1])
print(b[::-2])

#Membership
print("Saturday" in b)
print("Saturday" not in b)

weekend = ("Saturday","Sunday")

#Concatenation
print(b+weekend)

#Iteration
print(weekend*2)

# del weekend[1] #tuple is immutable
# weekend[1]="a"

# print(dir(a))

x = (10,20,30,10,50)

print(x.count(10))
print(x.count(140))
print(x.index(10))
print(x.index(10,1))
