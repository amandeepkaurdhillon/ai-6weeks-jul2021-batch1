options = '''
    Press 1: To add element
    Press 2: To view elements
    Press 3: To sum
    Press 4: To remove element
    Press 5: Exit
'''
print(options)
all=[]
while True:
    ch = input("Enter Your Choice: ")
    if ch == "1":
        n = input("Enter a number: ")
        if n.isdigit():
            all.append(int(n))
            print("{} added successfully!\n".format(n))
        else:
            print("Please enter numbers only!\n")
    elif ch=="2":
        print("Total: {}".format(len(all)))
        c=1
        for i in all:
            print("{}: {}".format(c,i))
            c+=1
        print("\n")
    elif ch=="3":
        s = 0
        count=0
        for i in all:
            s+=i 
            count+=1
        print("Sum of {} numbers is: {}\n".format(count,s))
    elif ch=="4":
        rm = input("Enter a number to remove: ")
        if rm.isdigit()==False:
            print("Please enter numbers only!\n") 
        else:
            n = int(rm)
            if n in all:
                all.remove(n)
                print("{} removed successfully!".format(n))
            else:
                print("{} not found in list!".format(n))
    elif ch=="5":
        break 
    else:
        print("Invalid Choice \n")

