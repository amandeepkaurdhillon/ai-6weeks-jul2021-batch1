student = {
    'name':'Harry Potter',
    'email':'potter@gmail.com',
    'roll_no':1,
    'age':23,
    'hobbies':['Gaming','Travelling','Reading Books'],
    'tech':{
        'front-end':['OpenCV','Tkinter'],
        'back-end':['Python','Sklearn','SQL']
    }
}
# Access Elements
print('{} is {} years old!'.format(student['name'],student['age']))
print(student['hobbies'][1])
print("Database: ", student['tech']['back-end'][2])

print(len(student))