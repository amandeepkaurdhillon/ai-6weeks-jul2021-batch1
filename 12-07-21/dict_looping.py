movie = {
    'name':'PK',
    'leading-actor':'Amir Khan',
    'revenue':'250 Crore',
    'year':2014,
}

# for i in movie:
#     print(i,"=>",movie[i])

for key,value in movie.items():
    print("{} : {}".format(key,value))

keys=movie.keys()
values = movie.values()

for x in zip(keys, values):
    print(x[0], x[1])