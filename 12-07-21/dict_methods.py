employee = {
    'name':'Mr. James Parker',
    'age':30,
    'DOJ':'01 July 2021',
    'salary':40000,
}
# print(dir(employee))
##Access
# print(employee['name']) 
# print(employee.get('name'))

## Update & Insert
# employee['age']=32
# employee['experience']='4+'
employee.update({'age':26,'experience':'3+'})

## Delete
# print(employee.pop('DOJ'),"removed!")
# employee.popitem()
# employee.clear()
# print(employee)
for i in employee:
    print('{} : {}'.format(i, employee[i]))

st = ['name','max marks', 'obt marks','attendance']
print({}.fromkeys(st,0))
