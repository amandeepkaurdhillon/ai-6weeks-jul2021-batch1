OOPS - Object Oriented Programming Structure
    object 
    class
    constructor
    destructor
    inheritance
    encapsulation
    polymorphism
    data abstraction

Constructor 
    - It is a special type of member function which called automatically on object creation
    - We use constructor to initialize values
    - In python, we use __init__ to create a constructor 
    - Constructor should return None

Destructor 
    - It is a special type of member function which called automatically on object creation
    - We use destructor to destroy values
    - In python, we use __del__ to create a constructor 

Inheritance:
    To create a new class by using features of existing class along with some new features
    Types:
        Simple Inheritance
        Multiple Inheritance
        Multilevel Inheritance
        Hierarchical Inheritance
        Hybrid Inheritance
        