class Circle:
    pi = 3.14

    def set_radius(self,r):
        self.r = r 

    def cal_area(self):
        print("R:{} Area:{}".format(
            self.r, self.pi*self.r**2))

c1 = Circle()
c1.set_radius(34)
c1.cal_area()

c2 = Circle()
c2.set_radius(10)
c2.cal_area()