#create a class
class Student:
    clz = "ABCD Engineering College"
    
    def intro(self):
        print("Inside function-",self.clz)
        print("I am a member function")
#Create an object
obj = Student()
#Access data members
print(obj.clz)
#Access member functions
obj.intro()
#Alter attributes using class
# obj.clz="XYZ college of management"
# del obj.clz
print(obj.clz)