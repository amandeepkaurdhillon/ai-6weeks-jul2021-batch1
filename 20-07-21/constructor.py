class Circle:
    pi = 3.14
    def __init__(self,rad=1):
        self.r=rad
        print("Constructor called!")
    def cal_area(self):
        print("R:{} Area:{}".format(self.r, self.pi*self.r**2))
    def cal_param(self):
        print("R:{} P:{}".format(self.r, 2*self.pi*self.r))
    def __del__(self):
        del self.r
        print("destructor called!")
c1 = Circle()
c2 = Circle(10)
c3 = Circle(4)
c1.cal_area()
c2.cal_area()
c3.cal_area()
c2.cal_param()