class Student:
    clz = "ABCD Engg. College"
    branch ="CSE"
    def __init__(self,name):
        self.n = name
        print("College Name:", self.clz)
        print("Branch:", self.branch)
class Account:
    basic = 10000
    def fee_details(self,due=0):
        print("Basic Fee: ", self.basic)
        if due==0:
            print("This student has no due fee!")
#Multiple Inheritance
class Library(Student, Account):
    def book_details(self, bname,isdt):
        print("Book Issued to ",format(self.n))
        print("Book Name: ",bname)
        print("Issued On: ",isdt)

obj = Library("John")
obj.fee_details()
obj.book_details("Python Programming","20-07-2021")