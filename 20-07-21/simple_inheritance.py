class Register:
    clz = "ABCD Group of colleges"
    basic_fee = 100000
    def __init__(self, name="Peter Parker", rno=1):
        self.n = name 
        self.r = rno 
        print(self.n, "regsitered successfully!")
#Single Inheritance
class Account(Register):
    def fee_details(self,pending):
        print("Here are fee details of: {}".format(self.n))
        print("Total Fee: Rs.{}/-".format(self.basic_fee))
        print("Pending Fee: Rs.{}/-".format(pending))
        print("Paid Fee: Rs.{}/-".format(self.basic_fee-pending))

s1 = Account("James",2)
# print(dir(s1))
s1.fee_details(1000)