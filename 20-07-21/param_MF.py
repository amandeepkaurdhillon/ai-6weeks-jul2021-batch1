class Student:
    clz = "ABCD college of Education"
    def intro(self, name, age, marks):
        self.n = name
        print("{}, who is studying at {}, is {} years old and secured {}% marks".format(
            name,self.clz, age, marks
        ))
    def fee_details(self,tf,pf):
        print("Here are fee details of: {}".format(self.n))
        print("Total Fee: Rs.{}/-".format(tf))
        print("Pending Fee: Rs.{}/-".format(pf))
s1 = Student()
s1.intro("Peter",19,80)
s2 = Student()
s2.intro("James",20,60)
s1.fee_details(10000,999)