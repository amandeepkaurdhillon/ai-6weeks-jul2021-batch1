import requests
URL = "https://restcountries.eu/rest/v2/all"
data = requests.get(URL).json()
c = input("Enter Country Name: ")
flag = False
for con in data:
    if c.lower() == con["name"].lower():    
        print("\t Name: {} ".format(con['name']))
        print("\t Capital: {} ".format(con['capital']))
        print("\t Area: {} ".format(con['area']))
        print("\t Population: {} ".format(con['population']))
        print("\t Borders: ")
        for n,b in enumerate(con['borders'],start=1):
            print("\t\t{}. {}".format(n,b))
        print("\t Languages: ")
        for nm,lang in enumerate(con["languages"],start=1):
            print("\t\t{}. {}".format(nm, lang['name']))
        flag = True 
if flag==False:
    print("Country Not found!")
    