# Enter City Name: Mohali 
#     Mohali (IN)
#     Temp: 34 C 
#     Clouds (Scattered Clouds)
## Images library: pip install pillow

import requests
from PIL import Image

city = input("Enter City Name: ")
URL = "https://api.openweathermap.org/data/2.5/weather?q={}&appid=e79a7d07c4e3bf54dd170836c3e88496&units=metric".format(city)
data = requests.get(URL).json()
if data["cod"]==200:
    print("\t{} ({})".format(data["name"],data["sys"]["country"]))
    print("\tTemp: {}°C".format(data["main"]["temp"]))
    print("\t{} ({})".format(data["weather"][0]["main"],data["weather"][0]["description"]))
    iconUrl = "https://openweathermap.org/img/wn/{}@2x.png".format(data["weather"][0]["icon"])
    print(iconUrl)
    im = Image.open(requests.get(iconUrl, stream=True).raw)
    im.show()
else:
    print("Incorrect city name!")