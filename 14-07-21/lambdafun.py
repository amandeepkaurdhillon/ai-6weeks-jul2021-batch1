def square(a):
    return a**2 
print(square(4))

b = lambda n1:n1**2
print(b(7))

c = lambda x,y:x+y
print(c(5,6))

def is_even(x):
    return x%2==0

print(is_even(91))

even = lambda n:n%2==0
print(even(77))

def check_evn(n):
    if n%2==0:
        return "Number is even"
    else:
        return "Number is Odd!"
print(check_evn(78))

ce = lambda z:"Number is even" if z%2==0 else "Number is odd!"

print(ce(7))