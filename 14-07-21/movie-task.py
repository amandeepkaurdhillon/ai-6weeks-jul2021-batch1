import random
op = '''
    Press 1: To add User
    Press 2: To add Admin
    Press 3: To add movie
    Press 4: To see all movies
    Press 5: To search movie
    Press 6: To delete
    Press 0: To exit
'''
all_movies = []
users = []
print(op)
while True:
    choice = input("Enter Your Choice: ")
    if choice == "1":
        name = input("Enter Your Name: ")
        u = {"username":name, "PIN":random.randint(1000,9999),"role":"U"}
        users.append(u)
        print("Dear {} !!! Thanks for registering with us\n Here is your PIN: {}".format(u["username"],u["PIN"]))
    elif choice=="2":
        name = input("Enter Your Name: ")
        u = {"username":name, "PIN":random.randint(1000,9999),"role":"A"}     
        users.append(u)
        print("Dear {} !!! Thanks for registering with us\n Here is your PIN: {}".format(u["username"],u["PIN"]))
   
    elif choice=="3":
        p = int(input("Enter your PIN: "))
        match = False
        for us in users:
            if us["PIN"]==p:
                match = True
                if us["role"]=="A":
                    print("Welcome {}!!!\n".format(us["username"]))
                    m_name = input("Enter Movie Name: ")
                    m_hero = input("Enter Hero name: ")
                    m_release_date = input("Enter Release Date: ")
                    m_seats = input("Enter number of seats: ")
                    movie = {
                        "name":m_name,"hero":m_hero,"release_date":m_release_date,
                        "total_seats":m_seats
                    }
                    all_movies.append(movie)
                    print("{} added successfully!\n".format(m_name))
                else:
                    print("Sorry {} You are not allowed to add movie!\n".format(us["username"]))
        if match==False:
            print("Sorry! You are not a registered user!!!\n")

    elif choice=="4":
        p = int(input("Enter your PIN: "))
        match = False
        for us in users:
            if us["PIN"]==p:
                match = True
                if us["role"]=="A":
                    print("********* Total: {} ***********".format(len(all_movies)))
                    c = 1
                    for i in all_movies:
                        print("\t{}. {} ({})\n".format(c,i["name"], i["release_date"]))
                        c += 1
                else:
                    print("Sorry {} You are not allowed to view all movies!\n".format(us["username"]))
        if match==False:
            print("Sorry! You are not a registered user!!!\n")

    elif choice=="5":
        p = int(input("Enter your PIN: "))
        match = False
        for us in users:
            if us["PIN"]==p:
                match = True
                if us["role"]=="U":
                    movie_to_search = input("Enter Movie Name To Search: ")
                    found = False
                    for i in all_movies:
                        if i["name"].lower()==movie_to_search.lower():
                            found=True
                            print("\t Movie Name: {}".format(i["name"]))
                            print("\t Hero Name: {}".format(i["hero"]))
                            print("\t Release Date: {}".format(i["release_date"]))
                            print("\t Total Seats: {}\n".format(i["total_seats"]))
                    if found==False:    
                        print("Sorry! Could not find '{}' in our database".format(movie_to_search))
                else:
                    print("Sorry Admin {} You are not allowed to search movie!\n".format(us["username"]))
        if match==False:
            print("Sorry! You are not a registered user!!!\n")
    
    elif choice == "6":
        p = int(input("Enter your PIN: "))
        match = False
        for us in users:
            if us["PIN"]==p:
                match = True
                if us["role"]=="A":
                    found = False
                    movie_to_delete = input("Enter movie to delete: ")
                    for i in all_movies:
                        if i["name"].lower()==movie_to_delete.lower():
                            found=True
                            all_movies.remove(i)
                            print("{} deleted successfully!\n".format(movie_to_delete))
                    if found==False:    
                        print("Sorry! Could not find '{}' in our database".format(movie_to_delete))
                else:
                    print("Sorry {} You are not allowed to delete movie!\n".format(us["username"]))
        if match==False:
            print("Sorry! You are not a registered user!!!\n")

    elif choice=="0":
        break 
    else:
        print("Invalid Choice!\n")