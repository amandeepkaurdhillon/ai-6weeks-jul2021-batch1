marks = [100,39,19,38,87,92,45,2,99,33]

def check(x):
    return x>=33

print(check(29))
# pas = list(filter(check, marks))
pas = list(filter(lambda x:x<33, marks))
print(pas)

st = "73a855887m43#%^a987654@n*&^%$"
rs = list(filter(lambda s:s.isalpha(), st))
print(rs)