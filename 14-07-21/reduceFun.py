from functools import reduce

# ls = [2,4,5,6,7,7,5,56]
ls = [100,200,10]

# r = reduce(lambda x,y:x+y, ls)
r = reduce(lambda x,y:x*y, ls)
print(r)