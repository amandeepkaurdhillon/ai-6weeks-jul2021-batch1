a = "Python "
b = "is a programming language"
names = "Peter Harry Aman"
# # Concatenation
# print(a+b)

# #Iteration
# print(a*4)

# #Membership
# print("is" in b)

n = input("Enter Name: ")
if n in names:
    print("Student Found!")
else:
    print("Not Found!")