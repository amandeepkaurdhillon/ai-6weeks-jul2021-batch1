a = 'Python is an easy language'
print(a)
#Indexing
print(a[1])
print(a[-1])
#Slicing
print(a[3:])
print(a[:3])
print(a[3:9])
print(a[-3:])
print(a[:-3])
a = "0123456789"
#Stepping
print(a[::2])
print(a[::1])
print(a[::3])
