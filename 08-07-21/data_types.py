a = 10
b = 10.5   
c = 10+5j 
d = '10'
e = [10,20,30] #mutable - we can edit,update,delete elements
f = (10,20,30) #immutable - we can't change its elements
g = {'name':'Aman','roll_no':1}
h = {100,200,300}
i = True
j = None
print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
print(type(f))
print(type(g))
print(type(h))
print(type(i))
print(type(j))
